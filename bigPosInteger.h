#ifndef LABEXAM1_BIGPOSINT_H
#define LABEXAM1_BIGPOSINT_H
#include <iostream>
#include <string>

class bigPosInteger {
    int * valueArray;
    int length;

public:
    bigPosInteger();
    explicit bigPosInteger(std::string value);
    explicit bigPosInteger(int valArr);
    bigPosInteger(const bigPosInteger& );//copy constructor
    virtual ~bigPosInteger();

    bigPosInteger operator+(const bigPosInteger&);
    bigPosInteger operator-(const bigPosInteger&);
    bigPosInteger operator*(const bigPosInteger&);
    bigPosInteger& operator=(const bigPosInteger&);
    bigPosInteger operator%(const bigPosInteger&);
    friend std::ostream& operator<< (std::ostream&, const bigPosInteger&);
    friend std::istream& operator>> (std::istream&, bigPosInteger&);
};

#endif //LABEXAM1_BIGPOSINT_H